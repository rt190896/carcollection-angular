import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from 'src/models/Users';
import { UserService } from 'src/Service/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  user: Users = new Users();
  constructor(private readonly userservice: UserService, private readonly router: Router) { }

  ngOnInit(): void {
  }

  Create() {
    this.userservice.signUp(this.user).subscribe(x => {
      this.router.navigateByUrl('/users');
    });
  }

}
