import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from 'src/models/Users';
import { UserService } from 'src/Service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  user: Users = new Users();
  message: string;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  Login() {
    this.userService.login(this.user).subscribe((x: number) => {
      debugger;
      if (x != 0) {
        localStorage.setItem("userId", x.toString());
        this.router.navigateByUrl('/cars');

      }
      else {
        debugger;
        this.message = "Invalid UserName/Password"

      }
    });
  }



}
