import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarListingComponent } from './car-listing/car-listing.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'cars-list' },
  { path: 'cars-list', component: CarListingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarManagementRoutingModule { }
