import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Carfilter, Cars } from 'src/models/Cars';
import { CarService } from 'src/Service/car.service';
@Component({
  selector: 'app-car-listing',
  templateUrl: './car-listing.component.html',
  styleUrls: ['./car-listing.component.css']
})
export class CarListingComponent implements OnInit {
  car: Cars = new Cars();

  carfilter: Carfilter = new Carfilter();

  cars: Cars[];

  modalRef: BsModalRef;

  constructor(private readonly carservice: CarService, private modalService: BsModalService) { }
  ngOnInit(): void {
    this.GetAll();
  }

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })

    );
  }

  Create() {

    if (this.car.id && this.car.id > 0) {
      this.carservice.update(this.car).subscribe(x => {
        this.car = new Cars();
        this.GetAll();
        this.modalRef.hide();
      });
    }
    else {
      this.car.UserId = Number(localStorage.getItem('userId'));
      this.carservice.create(this.car).subscribe(x => {
        this.car = new Cars();
        this.GetAll();
        this.modalRef.hide();
      });
    }
  }
  GetAll() {
    this.carfilter.userId = this.car.UserId = Number(localStorage.getItem('userId'));
    this.carservice.GetAll(this.carfilter).subscribe((x: Cars[]) => {
      this.cars = x
    });
  }
  GetCar(template: TemplateRef<any>, id: number) {
    this.carservice.Get(id).subscribe((x: Cars) => {
      debugger;
      this.car = x
      this.openModalWithClass(template);
    });
  }

  DeleteCar(id: number) {
    this.carservice.Delete(id).subscribe(() => {
      this.GetAll();
    });
  }

  // OpenModel() {
  //   this.car = new Cars()
  // }


}
