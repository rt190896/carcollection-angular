import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarListingComponent } from './car-listing/car-listing.component';
import { CarManagementRoutingModule } from './car-management-routing.module';


@NgModule({
  declarations: [CarListingComponent],
  imports: [
    CommonModule,
    FormsModule,
    CarManagementRoutingModule,
    ModalModule.forRoot(),
  ]
})
export class CarManagementModule { }
