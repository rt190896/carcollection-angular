import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Carfilter, Cars } from 'src/models/Cars';


@Injectable({
  providedIn: 'root'
})
export class CarService {


  constructor(private httpClient: HttpClient) { }

  create(car: Cars) {
    return this.httpClient.post('https://localhost:44367/api/car', car)
  }

  update(car: Cars) {
    return this.httpClient.put('https://localhost:44367/api/car', car)
  }

  GetAll(carfilter: Carfilter) {
    return this.httpClient.post('https://localhost:44367/api/car/getall', carfilter)
  }

  Get(id: number) {
    return this.httpClient.get('https://localhost:44367/api/car/' + id)
  }
  Delete(id: number) {
    return this.httpClient.delete('https://localhost:44367/api/car/' + id)
  }
}
