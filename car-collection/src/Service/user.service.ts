import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Users } from '../models/Users';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(private httpClient: HttpClient) { }

  login(user: Users) {
    return this.httpClient.post('https://localhost:44367/api/user/login', user)
  }

  signUp(user: Users) {
    return this.httpClient.post('https://localhost:44367/api/user', user)
  }

}
