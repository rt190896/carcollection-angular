export class Cars {
  public id: number;
  public brand: string;
  public model: string;
  public price: number;
  public new: boolean;
  public UserId: number;
}

export class Carfilter {
  public search: string;
  public userId: number;
}
